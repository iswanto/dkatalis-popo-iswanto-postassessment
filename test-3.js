import React from 'react';
import TextInput from './TextInput';

const InputField = {
    entityId :{
        type: 'text',
        placeHolder: 'Entity ID',
    },
    entityName :{
      type: 'text',
      placeHolder: 'Entity Name',
    },
    price :{
      type: 'text',
      placeHolder: 'Entity Price',
    },
    status :{
      type: 'text',
      placeHolder: 'Status',
    },
  };

export default class EntityGetGetGet extends React.Component {
    constructor(props) {
      super(props);
    }
  
    renderInput() {
        const renderInput = [];
        for (const input in InputField) {
          const currentInput = (
            <div key={input}>
              <TextInput name={input}
                type={InputField[input].type}
                placeholder={InputField[input].placeHolder} />
            </div>
          );
          renderInput.push(currentInput);
        }
        return renderInput;
      }

    render() {
      return (
       
        <div>
            {this.renderInput()}
            <button>submit</button>
        </div>
            
      );
    }
  }
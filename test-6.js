import React from 'react';
import TextInput from './TextInput';

const InputField = {
    entityId :{
        type: 'text',
        placeHolder: 'Entity ID',
    },
    entityName :{
      type: 'text',
      placeHolder: 'Entity Name',
    },
    price :{
      type: 'text',
      placeHolder: 'Entity Price',
    },
    status :{
      type: 'number',
      placeHolder: 'Status',
    },
  };

  const EntityList = [
    {
        entityId: '1',
        entityName: 'name',
        price: 20000,
        status : 0
    },
    {
        entityId: '2',
        entityName: 'name2',
        price: 30000,
        status : 1
    },
    {
        entityId: '3',
        entityName: 'name3',
        price: 50000,
        status : 2
    },
];

export default class EntityGetGetGet extends React.Component {
    constructor(props) {
      super(props);
      this.state = ({
        entityId: '',
        entityName: '',
        price: '',
        status : 0,
        renderEntity : [],
      });
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
      this.renderEntity = this.renderEntity.bind(this);
      this.deleteList = this.deleteList.bind(this);
    }

    componentDidMount(){
      this.renderEntity();
    }

    handleChange(event) {
      const currentState = this.state;
      currentState[event.target.name] = event.target.value;
      this.setState(currentState);
    }

    handleSubmit(event) {
      const entity = {
        entityId: this.state.entityId,
        entityName: this.state.entityName,
        price: this.state.price,
        status: this.state.status,
      };
      console.log("test");
      EntityList.push(entity);
      this.renderEntity();
    }
  
    renderInput() {
        const renderInput = [];
        for (const input in InputField) {
          const currentInput = (
            <div key={input}>
              <TextInput name={input}
                onChange={this.handleChange}
                type={InputField[input].type}
                placeholder={InputField[input].placeHolder} />
            </div>
          );
          renderInput.push(currentInput);
        }
        return renderInput;
      }

      deleteList(id){
        //to do : I still don't know how to delete item from list
        EntityList.pop();
        this.renderEntity();
      }

      renderEntity() {
        const renderEntityField = [];
        for (const entityField in EntityList) {
          const currentEntity = (
            <ul key={entityField} className="col-md-12">
                <li>entity id : {EntityList[entityField].entityId}</li>
                <li>entity name : {EntityList[entityField].entityName}</li>
                <li>price : {EntityList[entityField].price}</li>
                <li style={{
                    backgroundColor : EntityList[entityField].status == 0 ? "Red" 
                    :  EntityList[entityField].status == 1 ? "Orange" : "Green"
                }}>status : {EntityList[entityField].status}</li>
                <button onClick= {() => this.deleteList(EntityList[entityField].entityId)}>Hapus Aku!</button>
            </ul>
          );
          renderEntityField.push(currentEntity);
        }
        this.setState({renderEntity:renderEntityField})
      }

    render() {
      return (
        <div>
            {this.renderInput()}
            <button onClick={this.handleSubmit}>submit</button>
            {this.state.renderEntity}
        </div>
            
      );
    }
  }
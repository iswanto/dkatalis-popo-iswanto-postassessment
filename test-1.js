import React from 'react';

const EntityList = [
    {
        entityId: '1',
        entityName: 'name',
        price: 20000,
        status : 0
    },
    {
        entityId: '2',
        entityName: 'name2',
        price: 30000,
        status : 1
    },
    {
        entityId: '3',
        entityName: 'name3',
        price: 50000,
        status : 2
    },
];

export default class EntityGetGet extends React.Component {
    constructor(props) {
      super(props);
    }
  
    renderEntity() {
        const renderEntity = [];
        for (const entityField in EntityList) {
          const currentEntity = (
            <ul key={entityField} className="col-md-12">
                <li>entity id : {EntityList[entityField].entityId}</li>
                <li>entity name : {EntityList[entityField].entityName}</li>
                <li>price : {EntityList[entityField].price}</li>
                <li>status : {EntityList[entityField].status}</li>
            </ul>
          );
          renderEntity.push(currentEntity);
        }
        return renderEntity;
      }

    render() {
      return (
       
        <div>
            {this.renderEntity()}
        </div>
            
      );
    }
  }
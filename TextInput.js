
import React from 'react';

export default class TextInput extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <input name={this.props.name} onChange={this.props.onChange}
          type={this.props.type} placeholder={this.props.placeholder} />
      </div>

    );
  }
}
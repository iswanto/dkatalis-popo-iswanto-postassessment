package com.gin.ewallet.banana.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EntityServiceImpl implements EntityService {
	@Autowired
	private EntityRepository entityRepository;

	@Override
	public void save(Entity entity) {
		entityRepository.save(entity);
	}
}

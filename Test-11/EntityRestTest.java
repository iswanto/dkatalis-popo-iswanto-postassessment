package com.gin.ewallet.banana.repository;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.gin.ewallet.banana.constant.ApplicationConstants;
import com.gin.ewallet.banana.utils.ResponseModel;

@SpringBootTest
public class EntityRestTest {
	private MockMvc mockMvc;

	@InjectMocks
	private EntityRest entityRest;

	@Mock
	private EntityService entityService;
	
	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(entityRest).build();
	}
	
	@Test
	public void testGet() throws Exception {

		Entity entity = new Entity("id", "name", BigDecimal.valueOf(2000), 1);

		List<Entity> entities = new ArrayList<>();
		entities.add(entity);

		ResponseEntity<List<Entity>> response = ResponseEntity.status(HttpStatus.OK)
				.body(entities);

		Mockito.when(entityService.getAll()).thenReturn(entities);

		mockMvc.perform(get("/get").contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk());
	}

}

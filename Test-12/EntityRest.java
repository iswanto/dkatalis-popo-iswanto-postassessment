package com.gin.ewallet.banana.repository;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EntityRest {
	private static final Logger log = LoggerFactory.getLogger(EntityServiceImpl.class);
	
	@Autowired
	private EntityService entityService;

	@PostMapping("/save") 
	public ResponseEntity<String> save(@RequestBody Entity entity){
		log.info("save is being hit");
		entityService.save(entity);
		return ResponseEntity.ok("ok");
	}
	
	@GetMapping("/get") 
	public ResponseEntity<List<Entity>> getAll(){
		log.info("get is being hit");
		return ResponseEntity.ok(entityService.getAll());
	}
	
	@GetMapping("/getCount") 
	public ResponseEntity<List<EntityCount>> getCount(){
		log.info("get count is being hit");
		return ResponseEntity.ok(entityService.getCount());
	}
}

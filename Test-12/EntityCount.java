package com.gin.ewallet.banana.repository;

public class EntityCount {
	private int status;
	private int count;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
}

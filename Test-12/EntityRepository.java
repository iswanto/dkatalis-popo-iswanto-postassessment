package com.gin.ewallet.banana.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EntityRepository extends MongoRepository<Entity, String>, EntityRepoCustom{
	public List<Entity> findAll(Sort sort);
}

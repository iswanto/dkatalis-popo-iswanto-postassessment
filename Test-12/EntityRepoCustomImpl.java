package com.gin.ewallet.banana.repository;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.stereotype.Repository;

@Repository
public class EntityRepoCustomImpl implements EntityRepoCustom{
	private final MongoTemplate mongoTemplate;

	@Autowired
	public EntityRepoCustomImpl(MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}

	@Override
	public List<EntityCount> getEntityCount() {
		Aggregation aggregation = Aggregation.newAggregation(
				project().andExpression("status").as("status"),
				group("status")
						.sum("status").as("statusCount"));

		AggregationResults<EntityCount> result = mongoTemplate.aggregate(aggregation, Entity.class,
				EntityCount.class);
		return result.getMappedResults();
	}
}

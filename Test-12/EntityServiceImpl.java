package com.gin.ewallet.banana.repository;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


@Service
public class EntityServiceImpl implements EntityService {
	private static final Logger log = LoggerFactory.getLogger(EntityServiceImpl.class);
	@Autowired
	private EntityRepository entityRepository;

	@Override
	public void save(Entity entity) {
		log.info("service save is being hitted");
		entityRepository.save(entity);
	}
	
	@Override
	public List<Entity> getAll() {
		return entityRepository.findAll(new Sort(Sort.Direction.ASC,"entityName"));
	}
	
	@Override
	public List<EntityCount> getCount() {
		return entityRepository.getEntityCount();
	}
}

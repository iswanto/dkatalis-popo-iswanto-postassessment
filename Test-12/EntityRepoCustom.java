package com.gin.ewallet.banana.repository;

import java.util.List;

public interface EntityRepoCustom {
	public List<EntityCount> getEntityCount();
}

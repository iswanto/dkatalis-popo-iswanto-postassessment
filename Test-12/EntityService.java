package com.gin.ewallet.banana.repository;

import java.util.List;

public interface EntityService {

	public void save(Entity entity);
	public List<Entity> getAll();
	public List<EntityCount> getCount();
}

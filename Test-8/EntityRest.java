package com.gin.ewallet.banana.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EntityRest {
	
	@Autowired
	private EntityService entityService;

	@PostMapping("/save") 
	public ResponseEntity<String> save(@RequestBody Entity entity){
		entityService.save(entity);
		return ResponseEntity.ok("ok");
	}
	
	@PostMapping("/get") 
	public ResponseEntity<List<Entity>> getAll(){
		return ResponseEntity.ok(entityService.getAll());
	}
}

package com.gin.ewallet.banana.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class EntityServiceImpl implements EntityService {
	@Autowired
	private EntityRepository entityRepository;

	@Override
	public void save(Entity entity) {
		entityRepository.save(entity);
	}
	
	@Override
	public List<Entity> getAll() {
		return entityRepository.findAll(new Sort(Sort.Direction.ASC,"entityName"));
	}
}
